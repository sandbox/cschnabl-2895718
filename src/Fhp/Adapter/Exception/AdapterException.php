<?php

namespace Drupal\fints_hbci\Fhp\Adapter\Exception;

/**
 * Class AdapterException
 * @package Fhp\Adapter\Exception
 */
class AdapterException extends \Exception
{

}
