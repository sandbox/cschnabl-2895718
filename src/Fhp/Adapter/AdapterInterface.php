<?php

namespace Drupal\fints_hbci\Fhp\Adapter;

use Drupal\fints_hbci\Fhp\Message\AbstractMessage;

/**
 * Interface AdapterInterface
 * @package Fhp\Adapter
 */
interface AdapterInterface
{
    /**
     * @param AbstractMessage $message
     * @return string
     */
    public function send(AbstractMessage $message);
}
