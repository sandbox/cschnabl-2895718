<?php

/**
 * @file
 * Contains \Drupal\fints_hbci\Form\FintsHbciConfigForm.
 */

namespace Drupal\fints_hbci\Form;

use Drupal\Core\Form\ConfigFormBase;

use Drupal\Core\Form\FormStateInterface;

class FintsHbciConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */

  public function getFormId() {

    return 'fints_hbci_config_form';

  }

  /**
   * {@inheritdoc}
   */

  public function buildForm(array $form, FormStateInterface $form_state) {

    $form = parent::buildForm($form, $form_state);

    $config = $this->config('fints_hbci.settings');

    $form['accountId'] = array(

      '#type' => 'textfield',

      '#title' => $this->t('Kennung'),

      '#default_value' => $config->get('fints_hbci.accountId'),

      '#required' => TRUE,

    );

    $form['password'] = array(

      '#type' => 'password',

      '#title' => $this->t('Passwort'),

      '#default_value' => $config->get('fints_hbci.password'),

      '#required' => TRUE,

    );

    $node_types = \Drupal\node\Entity\NodeType::loadMultiple();

    $node_type_titles = array();

    foreach ($node_types as $machine_name => $val) {

      $node_type_titles[$machine_name] = $val->label();

    }

    $form['node_types'] = array(

      '#type' => 'checkboxes',

      '#title' => $this->t('Node Types'),

      '#options' => $node_type_titles,

      '#default_value' => $config->get('fints_hbci.node_types'),

    );

    return $form;

  }

  /**
   * {@inheritdoc}
   */

  public function submitForm(array &$form, FormStateInterface $form_state) {

    $config = $this->config('fints_hbci.settings');

    $config->set('fints_hbci.accountId', $form_state->getValue('accountId'));

    $config->set('fints_hbci.password', $form_state->getValue('password'));

    $config->set('fints_hbci.node_types', $form_state->getValue('node_types'));

    $config->save();

    return parent::submitForm($form, $form_state);

  }

  /**
   * {@inheritdoc}
   */

  protected function getEditableConfigNames() {

    return [

      'fints_hbci.settings',

    ];

  }

}