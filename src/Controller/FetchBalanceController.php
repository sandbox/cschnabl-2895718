<?php

namespace Drupal\fints_hbci\Controller;



use Drupal\Console\Bootstrap\Drupal;
use Drupal\Core\Controller\ControllerBase;


use Drupal\fints_hbci\Fhp;
use Fhp\Model\StatementOfAccount\Statement;
use Fhp\Model\StatementOfAccount\Transaction;

define('FHP_BANK_URL', '');                 # HBCI / FinTS Url can be found here: https://www.hbci-zka.de/institute/institut_auswahl.htm (use the PIN/TAN URL)
define('FHP_BANK_PORT', 443);               # HBCI / FinTS Port can be found here: https://www.hbci-zka.de/institute/institut_auswahl.htm
define('FHP_BANK_CODE', '');                # Your bank code / Bankleitzahl
define('FHP_ONLINE_BANKING_USERNAME', '');  # Your online banking username / alias
define('FHP_ONLINE_BANKING_PIN', '');       # Your online banking PIN (NOT! the pin of your bank card!)

class FetchBalanceController extends ControllerBase {

  /**
   * Display the markup.
   *
   * @return array
   */
  public function content() {
    dpm("get Balance");
    
    $this->getBalance();
    return array(
      '#type' => 'markup',
      '#markup' => "",
    );
  }
  
  public function getBalance() {
    $config = $this->config("fints_hbci.settings");
   
   print("tesst: " . $config->get("fints_hbci")['accountId']);


   
    $fints = new Fhp\FinTs(
      FHP_BANK_URL,
      FHP_BANK_PORT,
      FHP_BANK_CODE,
      FHP_ONLINE_BANKING_USERNAME,
      FHP_ONLINE_BANKING_PIN
    );
    $accounts = $fints->getSEPAAccounts();
    $oneAccount = $accounts[0];
    $saldo = $fints->getStatementOfAccount($oneAccount, new \DateTime('2017-07-01'), new \DateTime('2017-07-25'));
    dpm($saldo);
   
   
  }

}